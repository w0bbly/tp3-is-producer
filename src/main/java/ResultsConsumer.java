import json.JsonDeserializer;
import model.CountWeatherAlerts;
import model.CountWeatherEvents;
import model.CountWeatherEventsPerLocation;
import model.CountWeatherEventsType;
import model.MaxAndMinTempWeatherLocation;
import model.MaxAndMinTempWeatherStation;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.KafkaConsumer;

import java.time.Duration;
import java.util.Collections;
import java.util.Properties;

public class ResultsConsumer {
    public static void main(String[] args) {

        String resultsTopic = "results-topic";

        Properties props = new Properties(); //Assign localhost id
        props.put("bootstrap.servers", "localhost:9092");
        //Set acknowledgements for producer requests. props.put("acks", "all");
        //If the request fails, the producer can automatically retry,
        props.put("retries", 3);
        //Specify buffer size in config
        props.put("batch.size", 16384);
        //Reduce the no of requests less than 0
        props.put("linger.ms", 1);
        //The buffer.memory controls the total amount of memory available to the producer for buffering.
        props.put("buffer.memory", 33554432);
        props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        props.put("value.deserializer", JsonDeserializer.class.getName());

        //Q1
        Consumer<String, CountWeatherEvents> countWeatherEventsConsumer = new KafkaConsumer<>(props);
        countWeatherEventsConsumer.subscribe(Collections.singletonList(resultsTopic));

        while (true) {

            final int giveUp = 100;
            int noRecordsCount = 0;

            final var count = countWeatherEventsConsumer.poll(Duration.ofHours(1));

            if (count.count()==0) {
                noRecordsCount++;
                if (noRecordsCount > giveUp) break;
                else continue;
            }

            count.forEach(record -> {
                System.out.println("Station Q1 -> " + record.value().getStationName());
                System.out.println("Count Q1 -> " + record.value().getCount());
            });

            countWeatherEventsConsumer.commitAsync();
        }

        countWeatherEventsConsumer.close();
        System.out.println("Q1 done");

        //Q2
        Consumer<String, CountWeatherEventsPerLocation> countWeatherEventsPerLocationConsumer = new KafkaConsumer<>(props);
        countWeatherEventsPerLocationConsumer.subscribe(Collections.singletonList(resultsTopic));

        while (true) {

            final int giveUp = 100;
            int noRecordsCount = 0;

            final var count = countWeatherEventsPerLocationConsumer.poll(Duration.ofHours(1));

            if (count.count()==0) {
                noRecordsCount++;
                if (noRecordsCount > giveUp) break;
                else continue;
            }

            count.forEach(record -> {
                System.out.println("Station Location Q2 -> " + record.value().getStationLocation());
                System.out.println("Count Q2 -> " + record.value().getCount());
            });

            countWeatherEventsPerLocationConsumer.commitAsync();
        }

        countWeatherEventsPerLocationConsumer.close();
        System.out.println("Q2 done");

        //Q3
        Consumer<String, MaxAndMinTempWeatherStation> maxAndMinTempWeatherStationConsumer = new KafkaConsumer<>(props);
        maxAndMinTempWeatherStationConsumer.subscribe(Collections.singletonList(resultsTopic));

        while (true) {

            final int giveUp = 100;
            int noRecordsCount = 0;

            final var maxAndMinTempWeatherStationConsumerRecords = maxAndMinTempWeatherStationConsumer.poll(Duration.ofHours(1));

            if (maxAndMinTempWeatherStationConsumerRecords.count()==0) {
                noRecordsCount++;
                if (noRecordsCount > giveUp) break;
                else continue;
            }

            maxAndMinTempWeatherStationConsumerRecords.forEach(record -> {
                System.out.println("Station Q3 -> " + record.value().getWeatherStationName());
                System.out.println("Min Q3 -> " + record.value().getMinTemp());
                System.out.println("Max Q3 -> " + record.value().getMaxTemp());
            });

            maxAndMinTempWeatherStationConsumer.commitAsync();
        }

        maxAndMinTempWeatherStationConsumer.close();
        System.out.println("Q3 done");

        //Q4
        Consumer<String, MaxAndMinTempWeatherLocation> maxAndMinTempWeatherLocationConsumer = new KafkaConsumer<>(props);
        maxAndMinTempWeatherLocationConsumer.subscribe(Collections.singletonList(resultsTopic));

        while (true) {

            final int giveUp = 100;
            int noRecordsCount = 0;

            final var maxAndMinTempWeatherLocationConsumerRecords = maxAndMinTempWeatherLocationConsumer.poll(Duration.ofHours(1));

            if (maxAndMinTempWeatherLocationConsumerRecords.count()==0) {
                noRecordsCount++;
                if (noRecordsCount > giveUp) break;
                else continue;
            }

            maxAndMinTempWeatherLocationConsumerRecords.forEach(record -> {
                System.out.println("Location Q4 -> " + record.value().getWeatherLocationName());
                System.out.println("Min Q4 -> " + record.value().getMinTemp());
                System.out.println("Max Q4 -> " + record.value().getMaxTemp());
            });

            maxAndMinTempWeatherLocationConsumer.commitAsync();
        }

        maxAndMinTempWeatherLocationConsumer.close();
        System.out.println("Q4 done");

        //Q5
        Consumer<String, CountWeatherAlerts> countWeatherAlertsConsumer = new KafkaConsumer<>(props);
        countWeatherAlertsConsumer.subscribe(Collections.singletonList(resultsTopic));

        while (true) {

            final int giveUp = 100;
            int noRecordsCount = 0;

            final var consumerRecords = countWeatherAlertsConsumer.poll(Duration.ofHours(1));

            if (consumerRecords.count()==0) {
                noRecordsCount++;
                if (noRecordsCount > giveUp) break;
                else continue;
            }

            consumerRecords.forEach(record -> {
                System.out.println("Station Q5 -> " + record.value().getStationName());
                System.out.println("Count Q5 -> " + record.value().getCount());
            });

            countWeatherAlertsConsumer.commitAsync();
        }

        countWeatherAlertsConsumer.close();
        System.out.println("Q5 done");

        //Q6
        Consumer<String, CountWeatherEventsType> countWeatherEventsTypeConsumer = new KafkaConsumer<>(props);
        countWeatherEventsTypeConsumer.subscribe(Collections.singletonList(resultsTopic));

        while (true) {

            final int giveUp = 100;
            int noRecordsCount = 0;

            final var eventsTypeConsumerRecords = countWeatherEventsTypeConsumer.poll(Duration.ofHours(1));

            if (eventsTypeConsumerRecords.count()==0) {
                noRecordsCount++;
                if (noRecordsCount > giveUp) break;
                else continue;
            }

            eventsTypeConsumerRecords.forEach(record -> {
                System.out.println("Alert Q6 -> " + record.value().getAlertType());
                System.out.println("Count Q6 -> " + record.value().getCount());
            });

            countWeatherEventsTypeConsumer.commitAsync();
        }

        countWeatherEventsTypeConsumer.close();
        System.out.println("Q6 done");
    }
}
