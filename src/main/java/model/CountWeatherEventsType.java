package model;

public class CountWeatherEventsType {
    private AlertType alertType;
    private Long count;

    public CountWeatherEventsType() {
    }

    public CountWeatherEventsType(AlertType alertType, Long count) {
        this.alertType = alertType;
        this.count = count;
    }

    public AlertType getAlertType() {
        return alertType;
    }

    public void setAlertType(AlertType alertType) {
        this.alertType = alertType;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    @Override
    public String toString() {
        return "CountWeatherEventsType{" +
                "alertType=" + alertType +
                ", count=" + count +
                '}';
    }
}
