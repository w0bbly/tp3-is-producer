package model;

public class CountWeatherAlerts {
    private String stationName;
    private Long count;

    public CountWeatherAlerts() {
    }

    public CountWeatherAlerts(String stationName, Long count) {
        this.stationName = stationName;
        this.count = count;
    }

    public String getStationName() {
        return stationName;
    }

    public void setStationName(String stationName) {
        this.stationName = stationName;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    @Override
    public String toString() {
        return "CountWeatherAlerts{" +
                "stationName='" + stationName + '\'' +
                ", count=" + count +
                '}';
    }
}
