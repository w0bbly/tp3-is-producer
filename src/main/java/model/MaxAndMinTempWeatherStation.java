package model;

public class MaxAndMinTempWeatherStation {
    private String weatherStationName;
    private Long minTemp = 0L;
    private Long maxTemp = 0L;

    public MaxAndMinTempWeatherStation() {
    }

    public MaxAndMinTempWeatherStation(String weatherStationName, Long minTemp, Long maxTemp) {
        this.weatherStationName = weatherStationName;
        this.minTemp = minTemp;
        this.maxTemp = maxTemp;
    }

    public String getWeatherStationName() {
        return weatherStationName;
    }

    public void setWeatherStationName(String weatherStationName) {
        this.weatherStationName = weatherStationName;
    }

    public Long getMinTemp() {
        return minTemp;
    }

    public void setMinTemp(Long minTemp) {
        this.minTemp = minTemp;
    }

    public Long getMaxTemp() {
        return maxTemp;
    }

    public void setMaxTemp(Long maxTemp) {
        this.maxTemp = maxTemp;
    }

    @Override
    public String toString() {
        return "MaxAndMinTempWeatherStation{" +
                "weatherStationName='" + weatherStationName + '\'' +
                ", minTemp=" + minTemp +
                ", maxTemp=" + maxTemp +
                '}';
    }
}
