package model;

public class CountWeatherEvents {
    private String stationName;
    private Long count;

    public CountWeatherEvents() {
    }

    public CountWeatherEvents(String stationName, Long count) {
        this.stationName = stationName;
        this.count = count;
    }

    public String getStationName() {
        return stationName;
    }

    public void setStationName(String stationName) {
        this.stationName = stationName;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    @Override
    public String toString() {
        return "CountWeatherEvents{" +
                "stationName='" + stationName + '\'' +
                ", count=" + count +
                '}';
    }
}
