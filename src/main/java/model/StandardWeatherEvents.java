package model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class StandardWeatherEvents {
    @JsonProperty
    String location;

    @JsonProperty
    Double temperature;

    public StandardWeatherEvents() {
    }

    public StandardWeatherEvents(String location, Double temperature) {
        this.location = location;
        this.temperature = temperature;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Double getTemperature() {
        return temperature;
    }

    public void setTemperature(Double temperature) {
        this.temperature = temperature;
    }

    @Override
    public String toString() {
        return "StandardWeatherEvents{" +
                "location='" + location + '\'' +
                ", temperature=" + temperature +
                '}';
    }
}
