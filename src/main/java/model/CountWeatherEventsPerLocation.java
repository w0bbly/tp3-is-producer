package model;

public class CountWeatherEventsPerLocation {
    private String stationLocation;
    private Long count;

    public CountWeatherEventsPerLocation() {
    }

    public CountWeatherEventsPerLocation(String stationLocation, Long count) {
        this.stationLocation = stationLocation;
        this.count = count;
    }

    public String getStationLocation() {
        return stationLocation;
    }

    public void setStationLocation(String stationLocation) {
        this.stationLocation = stationLocation;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    @Override
    public String toString() {
        return "CountWeatherEventsPerLocation{" +
                "stationLocation='" + stationLocation + '\'' +
                ", count=" + count +
                '}';
    }
}


