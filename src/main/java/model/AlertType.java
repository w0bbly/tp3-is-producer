package model;

import java.util.Random;

public enum AlertType {
    RED,
    GREEN;

    private static final Random PRNG = new Random();

    public static AlertType randomAlert()  {
        AlertType[] alertTypes = values();
        return alertTypes[PRNG.nextInt(alertTypes.length)];
    }
}
