package model;

public class MaxAndMinTempWeatherLocation {
    private String weatherLocationName;
    private Long minTemp = 0L;
    private Long maxTemp = 0L;

    public MaxAndMinTempWeatherLocation(String weatherLocationName, Long minTemp, Long maxTemp) {
        this.weatherLocationName = weatherLocationName;
        this.minTemp = minTemp;
        this.maxTemp = maxTemp;
    }

    public MaxAndMinTempWeatherLocation() {
    }

    public String getWeatherLocationName() {
        return weatherLocationName;
    }

    public void setWeatherLocationName(String weatherLocationName) {
        this.weatherLocationName = weatherLocationName;
    }

    public Long getMinTemp() {
        return minTemp;
    }

    public void setMinTemp(Long minTemp) {
        this.minTemp = minTemp;
    }

    public Long getMaxTemp() {
        return maxTemp;
    }

    public void setMaxTemp(Long maxTemp) {
        this.maxTemp = maxTemp;
    }

    @Override
    public String toString() {
        return "MaxAndMinTempWeatherLocation{" +
                "weatherLocationName='" + weatherLocationName + '\'' +
                ", minTemp=" + minTemp +
                ", maxTemp=" + maxTemp +
                '}';
    }
}
