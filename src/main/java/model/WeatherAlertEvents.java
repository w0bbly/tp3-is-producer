package model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class WeatherAlertEvents {

    @JsonProperty
    String location;

    @JsonProperty
    AlertType type;

    public WeatherAlertEvents() {
    }

    public WeatherAlertEvents(String location, AlertType type) {
        this.location = location;
        this.type = type;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public AlertType getType() {
        return type;
    }

    public void setType(AlertType type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "WeatherAlertEvents{" +
                "location='" + location + '\'' +
                ", type=" + type +
                '}';
    }
}
