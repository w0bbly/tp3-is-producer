import json.JsonSerializer;
import model.AlertType;
import model.StandardWeatherEvents;
import model.WeatherAlertEvents;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Random;

public class WeatherStationProducer {
    public static void main(String[] args) {

        String standardWeatherTopic = "standard-weather-topic";
        String weatherAlertsTopic = "weather-alerts-topic";

        // create instance for properties to access producer configs
        Properties props = new Properties(); //Assign localhost id
        props.put("bootstrap.servers", "localhost:9092");
        //Set acknowledgements for producer requests. props.put("acks", "all");
        //If the request fails, the producer can automatically retry,
        props.put("retries", 3);
        //Specify buffer size in config
        props.put("batch.size", 16384);
        //Reduce the no of requests less than 0
        props.put("linger.ms", 1);
        //The buffer.memory controls the total amount of memory available to the producer for buffering.
        props.put("buffer.memory", 33554432);
        props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        props.put("value.serializer", JsonSerializer.class.getName());

        //Write 10000 results to the Standard Weather Topic
        Producer<String, StandardWeatherEvents> producerStandardWeatherTopic = new KafkaProducer<>(props);

        List<String> allCities = new ArrayList<>();
        allCities.add("Coimbra");
        allCities.add("Porto");
        allCities.add("Lisboa");

        List<String> coimbraLocations = new ArrayList<>();
        coimbraLocations.add("Baixa");
        coimbraLocations.add("Alta");
        coimbraLocations.add("Praça da Républica");
        coimbraLocations.add("Celas");
        coimbraLocations.add("Olivais");
        coimbraLocations.add("Vale das Flores");
        coimbraLocations.add("Santa Clara");

        List<String> portoLocations = new ArrayList<>();
        portoLocations.add("Gaia");
        portoLocations.add("Clérigos");
        portoLocations.add("Aliados");
        portoLocations.add("Rio Tinto");
        portoLocations.add("Matosinhos");
        portoLocations.add("Ribeira");
        portoLocations.add("Maia");

        List<String> lisboaLocations = new ArrayList<>();
        lisboaLocations.add("Belem");
        lisboaLocations.add("Alges");
        lisboaLocations.add("Oriente");
        lisboaLocations.add("Sete Rios");
        lisboaLocations.add("Parque das Nações");
        lisboaLocations.add("Estoril");
        lisboaLocations.add("Marquês de Pombal");

        Map<String, List<String>> cities = new HashMap<>();

        cities.put("Coimbra", coimbraLocations);
        cities.put("Porto", portoLocations);
        cities.put("Lisboa", lisboaLocations);

        for(int i = 0; i < 10000; i++) {
            //Randomize city
            Random randomCity = new Random();
            int r = randomCity.nextInt(3);

            //Randomize Location
            Random randomLocation = new Random();
            int rL = randomLocation.nextInt(7);

            //Randomize temperature
            Random randomTemperature = new Random();
            double rT = randomTemperature.nextDouble(-21.0, 41.0);

            StandardWeatherEvents standardWeatherEvents = new StandardWeatherEvents(cities.get(allCities.get(r)).get(rL), rT);
            producerStandardWeatherTopic.send(new ProducerRecord<>(standardWeatherTopic, allCities.get(r), standardWeatherEvents));
        }

        producerStandardWeatherTopic.close();

        //Write 10000 results to the Weather Alert Events Topic
        Producer<String, WeatherAlertEvents> weatherAlertEventsProducer = new KafkaProducer<>(props);

        for(int i = 0; i < 10000; i++) {
            //Randomize city
            Random randomCity = new Random();
            int r = randomCity.nextInt(2);

            //Randomize Location
            Random randomLocation = new Random();
            int rL = randomLocation.nextInt(7);

            //Randomize temperature
            AlertType alertType = AlertType.randomAlert();

            WeatherAlertEvents weatherAlertEvents = new WeatherAlertEvents(cities.get(allCities.get(r)).get(rL), alertType);
            weatherAlertEventsProducer.send(new ProducerRecord<>(weatherAlertsTopic, allCities.get(r), weatherAlertEvents));
        }

        weatherAlertEventsProducer.close();
    }
}
